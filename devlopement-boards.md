# developement-boards

## boards supported by zephyr

https://docs.zephyrproject.org/latest/boards/index.html#boards


for all microcontroller lines different boards

### ARM 

ARM (stylised in lowercase as arm, formerly an acronym for Advanced RISC Machines and originally Acorn RISC Machine) is a family of reduced instruction set computer (RISC) instruction set architectures for computer processors, configured for various environments. 1

#### STM32 microcontroller

STM32G031F8P6/

https://www.digikey.es/en/products/detail/stmicroelectronics/STM32G031F8P6/10326693

STM32 Nucleo Boards

https://www.digikey.es/en/products/detail/stmicroelectronics/NUCLEO-F401RE/4695525


Nucleo Boards using the STM32 Microcontrollers

### AVR microcontrollers

AVR is a family of microcontrollers developed since 1996 by Atmel, acquired by Microchip Technology in 2016. These are modified Harvard architecture 8-bit RISC single-chip microcontrollers. AVR was one of the first microcontroller families to use on-chip flash memory for program storage, as opposed to one-time programmable ROM, EPROM, or EEPROM used by other microcontrollers at the time. 


#### ATtiny85

8-bit                            
Microcontroller 
with 2/4/8K 
Bytes In-System
Programmable 
Flash

attiny85-easy-flashing-through-arduino
https://medium.com/jungletronics/attiny85-easy-flashing-through-arduino-b5f896c48189

void setup() {
  // initialize digital ATtiny pin 0 as an output.
  pinMode(0, OUTPUT);
}

// the loop function runs over and over again forever
void loop() {
  digitalWrite(0, HIGH);   // turn the LED on (HIGH is the voltage level)
  delay(100);              // wait for a 100 ms
  digitalWrite(0, LOW);    // turn the LED off by making the voltage LOW
  delay(100);              // wait for  a 100 ms
}

OLIMEXINO-328

#### STK600 starter kit