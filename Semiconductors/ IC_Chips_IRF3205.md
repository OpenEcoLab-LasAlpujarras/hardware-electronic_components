[datasheet](https://pdf1.alldatasheet.com/datasheet-pdf/view/68131/IRF/IRF3205.html)

## Description

Specifically designed for Automotive applications, this HEXFET® power MOSFET has a 200°C max operating temperature with a Stripe Planar design that utilizes the latest processing techniques to achieve extremely low on-resistance per silicon area. Additional features of this HEXFET® power MOSFET are fast switching speed and improved repetitive avalanche rating.
The continuing technology leadership of Internationl Rectifier provides 200°C operating temperature in a plastic package. At high ambient temperatures, the IRF1704 can carry up to 20% more current than similar 175 °C Tj max devices in the same package outline. This makes this part ideal for existing and emerging under-the-hood automotive applications such as Electric Power Steering (EPS), Fuel / Water Pump Control and wide variety of other applications.

## Benefits

• 200°C Operaing Temperature
• Advanced Process Technology
• Ultra Low On-Resistance
• Dynamic dv/dt Rating
• Fast Switching
• Repetitive Avalanche Allowed up to Tj Max
• Automotive Qualified (Q101)


## projects

### motor controll



 