## TC4069UBP



#### Description:

Inverters CONV-DIP,DISCON(07-10)/PHASE-OUT(10-01)/OBSOLETE(10-04),


TC4069UB Hex Inverter
TC4069UB contains six circuits of inverters. Since the internal
circuit is composed of a single stage inverter, this is suitable for
the applications of CR oscillator circuits, crystal oscillator circuits
and linear amplifiers in addition to its application as inverters.
Because of one stage gate configuration, the propagation time
has been reduced.

CR oscillator circuits
crystal oscillator circuits
linear amplifiers
inverter

