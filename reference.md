## reference

(open-hardware-anderswallin)[http://www.anderswallin.net/open-hardware/]

    One Inch Photodetector
    SFP-loopback-board
    SFP-breakout-board (2020-02)
    SFP-board 2018-03
    PICDIV, 2018 update
    Frequency distribution amplifier (2017-2018)
    Pulse distribution amplifier (2017-2018)
    Isolated pulse distribution amplifier
    RF SP8T 1+ GHz multiplexer
    DIY DSOXLAN (2015)
    Constant-current TEC drive (2014)
    HVDC amp (2013)
    PICDIV frequency divider (2016)
    PID controller (2014)
    Misc