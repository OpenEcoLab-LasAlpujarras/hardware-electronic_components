# clamp meter

## principle

### current-transformer

https://www.electronics-tutorials.ws/transformer/current-transformer.html

## hardware

### SNS-CURRENT-CT013-100A	

https://www.olimex.com/Products/Components/Sensors/Current/SNS-CURRENT-CT013-100A/

 SNS-CURRENT-CT013-100A is split core clamp current transformer which is good for sensing currents up to 100A

## DIY


### Make Your Own Current Clamp Probe
https://hackaday.com/2018/01/27/make-your-own-current-clamp-probe/


### diy-real-energy-meter-with-arduino-and-esp8266-29cae6
https://www.hackster.io/mcmchris/diy-real-energy-meter-with-arduino-and-esp8266-29cae6

### Safety:
Current transformers should never operate without burden resistor, as if left open very high voltage may generate through second winding which to cause sparking. So never use this sensor without connecting it to proper circuit with burden resistor!

https://www.electrical4u.net/calculator/what-is-ct-burden-resistor-burden-resistor-calculator/

The current transformers must always have burden resistor connected to the second coil, the purpose is to measure the voltage drop on this resistor which will be proportional to the input current. This burden resistor value also should be low enough to prevent core saturation.

## reference

https://learn.openenergymonitor.org/electricity-monitoring/ct-sensors/how-to-build-an-arduino-energy-monitor-measuring-current-only?redirected=true

https://learn.openenergymonitor.org/electricity-monitoring/ct-sensors/interface-with-arduino?redirected=true

https://learn.openenergymonitor.org/electricity-monitoring/ct-sensors/introduction?redirected=true