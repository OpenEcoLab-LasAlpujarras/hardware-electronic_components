## Passive Components

### categories

Antennas

Capacitors

Crystals, Oscillators & Resonators

EMI Filters & Protection

Fixed Resistors

Inductors

Passive Components Kits

Rotary Encoders

Sounder, Buzzer & Microphone Components

Surge Protection Components

Variable Resistors