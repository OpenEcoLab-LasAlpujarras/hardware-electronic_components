# hardware-electronic_components

This repository stores all kind of hardware and electronic components.

You will find all kind of things like,

    -technical files
    -link to manufactures
    -link to supplier
    -schematics

## Places to find informatioo about components

### Database Search Window

https://www.web-bcs.com

https://www.datasheets.com

[LibreSolar_mppt-1210-hus.md](LibreSolar_mppt-1210-hus.md)

### supplier
-------

Electronic supplyer

--------

#### shops

caballeros electronicos (cordoba)


#### online

##### [Mouser->https://www.mouser.es/] 

Circuit Protection, Connectors, Electromechanical, Embedded Solutions, Enclosures, Engineering Tools, Industrial Automation, LED Lighting, Memory & Data Storage, Opto-electronics ,Passive Components, Power, Semiconductors, Sensors, Test & Measurement, Thermal Management, Tools & Supplies, Wire & Cable.

##### Interesting products

###### Resistor

[Thick Film Resistors - SMD 100K OHM 1%->https://www.mouser.es/ProductDetail/Yageo/RC0603FR-07100KL?qs=%2Fha2pyFaduhXXNW8qwNUNke01QZvXPmnTMg%2F%2FEocqaELH84Y7InL5g%3D%3D]

#### [Nkon->https://eu.nkon.nl/]

##### Interesting products

###### Battery management systems

###### Li ion cells

[Sony / Murata US18650VTC6 3000mAh - 30A->https://eu.nkon.nl/products/sony-us18650-vtc6.html]

#### [Digikey->https://www.digikey.es/]

##### Interesting products

#### [Farnell->https://es.farnell.com/]

##### Interesting products


#### [More onlineshops->https://www.eleccircuit.com/electronic-supplies-store/]

second hand/gift

----------

[milanuncios->https://www.milanuncios.com/]
[Wallapop->https://es.wallapop.com]

recycling

----------

punto limpio



### Electronics Components, Power & Connectors

#### Batteries & Chargers

    Battery & Charger Accessories(439)
    Battery Chargers & Power Banks(285)
    Non-Rechargeable Batteries(453)
    Rechargeable Batteries(526)

#### Connectors

    AV Connectors(1158)
    Automotive Electrical Connectors(3555)
    Backplane & Rack Connectors(1499)
    Circular Connectors(16483)
    IC Sockets & Adapters(709)
    Mains & DC Power Connectors(4315)
    Network & Telecom Connectors(2399)
    PCB Connectors & Wire Housings(46564)
    Power Connectors(7620)
    RF Coaxial Connectors & Adapters(4886)
    Terminal Blocks(20748)
    USB, D Sub & Computing Connectors(9244)
    Wire Terminals & Splices(5929)


#### Displays & Optoelectronics

    Displays & Industrial Monitors(2044)
    Fibre Optic Components(192)
    IR Emitters & Receivers(204)
    Indicators & Indicator Components(3606)
    LED Indicators & Accessories(456)
    LED Lighting System Components(4649)
    LEDs & LED Accessories(7280)
    Laser Modules & Components(228)
    Optocouplers, Photodetectors & Photointerrupters(4548)


#### ESD Control, Cleanroom & PCB Prototyping

    ESD Control & Clean Room(792)
    PCB Etching, Preparation & Developing(63)
    PCB Handling(94)
    PCB Spacers, Pillars & Supports(835)
    PCB Tools(137)
    Prototyping Boards(822)
    
#### Passive Components

    Antennas
    Capacitors
    Crystals, Oscillators & Resonators
    EMI Filters & Protection
    Fixed Resistors
    Inductors
    Passive Components Kits(
    Rotary Encoders
    Sounder, Buzzer & Microphone Components
    Surge Protection Components
    Variable Resistors

#### Power Supplies & Transformers

    DC-AC Power Inverters(161)
    DC-DC Converters(14611)
    Portable Generators & Accessories(10)
    Power Conditioners(33)
    Power Supplies - PSUs(7764)
    Power over Ethernet - PoE(14)
    Renewable Energy(177)
    Transformers(3046)
    Uninterruptible Power Supplies - UPS(416)

#### Raspberry Pi, Arduino & Development Tools

    Arduino Shop(238)
    BBC micro:bit Shop(172)
    Development Tools & Single Board Computers(5006)
    Raspberry Pi Shop(482)
    STEM education(76)

#### Semiconductors

    Amplifiers & Comparators(6866)
    Audio & Video ICs(138)
    Chip Programmers & Debuggers(131)
    Clock, Timing & Frequency ICs(1212)
    Communication & Wireless Module ICs(1140)
    Data Converters(2819)
    Discrete Semiconductors(37754)
    Interface ICs(4715)
    Logic ICs(6102)
    Memory Chips(2732)
    Power Management ICs(16328)
    Processors & Microcontrollers(7234)
    Programmable Logic ICs(201)
    Sensor ICs(3060)


### Electrical, Automation & Cables

#### Automation & Control Gear

    Automation Signalling(3735)
    Contactors & Auxiliary Contacts(6611)
    Electric Motors(7822)
    Industrial Robots(16)
    PLCs, HMIs & Industrial Computing(5587)
    Process Control(6293)
    Sensors(18305)
    Solenoids(170)

#### Cables & Wires
#### Enclosures & Server Racks
#### Fuses & Circuit Breakers
#### HVAC, Fans & Thermal Management
#### Lighting
#### Relays
#### Switches

### Mechanical Products & Tools

### IT, Test & Safety Equipment

#### Computing & Peripherals

    3D Printing & Scanning
    Audio & Video
    Barcode Readers & Accessories
    Data Storage & Memory
    KVM, Splitters & Extenders
    Keyboards & Mice
    Mobile Computing
    Networking & Connectivity
    Optical Drives & Media
    Photography & Imaging
    Printers & Printing Supplies
    Software
    Telecommunications
    Wireless Components & Modules

#### Facilities Cleaning & Maintenance

    Brushes
    Cleaning Equipment
    Cleaning Sprays & Fluids
    Electronics Cleaners & Protective Coatings
    Floor Cleaning
    Greases, Oils & Lubricants
    Paint & Painting Supplies
    Washroom Equipment & Supplies
    Waste & Recycling
    Wipes & Cloths

#### Office Supplies

    Audio Equipment
    Books & Training Materials
    Clocks, Timers & Stopwatches
    Food & Beverage Preparation
    Furniture & Desk Accessories
    Labelling, Binding & Laminating
    Microphones & Headphones
    Notice Boards, Wall Planners & Posters
    Package Strapping, Sealing & Opening
    Paper Supplies
    Presentation Tools
    Speakers & Components
    Video & Visuals Equipment

#### Personal Protective Equipment & Workwear

    Arm & Knee Protection
    Ear Protection
    Eye & Face Protection
    Fall Protection
    Hand Protection
    Head Protection
    Hi Vis Clothing
    Protective Clothing
    Respiratory Protection
    Safety Footwear
    Workwear

#### Test & Measurement
    Bench Power Supplies & Sources
    Data Acquisition & Logging
    Electrical Test & Measurement
        [RS](https://uk.rs-online.com/web/c/test-measurement/electrical-test-measurement/)
        [clamp meter](hardware-electronic_components/Electrical-Test-Measurement/clamp-meter.md)
    Electronic Component Testing
    Engineering Test & Measurement
    Environmental Test & Measurement
    Lab Equipment
    Linear Test & Measurement
    Multimeters & Accessories
